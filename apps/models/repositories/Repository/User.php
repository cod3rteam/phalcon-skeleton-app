<?php
namespace PhalconSkeletonApp\Models\Repositories\Repository;

use PhalconSkeletonApp\Models\Entities\User as EntityUser;

class User
{
    public function getLast()
    {
        return EntityUser::find()->toArray();
    }
}
