<?php
namespace PhalconSkeletonApp\Models\Services\Service;
use PhalconSkeletonApp\Models\Repositories\Repositories as Repositories;

class User
{
    public function getLast()
    {
        return Repositories::getRepository('User')->getLast();
    }
}