SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP DATABASE IF EXISTS `phalcon-tests`;

CREATE SCHEMA IF NOT EXISTS `phalcon-tests` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `phalcon-tests`;

-- -----------------------------------------------------
-- Table `phalcon-tests`.`user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `phalcon-tests`.`user` (
  `iduser` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(200) NOT NULL default '',
  `username` VARCHAR(45) NOT NULL default '',
  `email` VARCHAR(100) NOT NULL default '',
  `password` VARCHAR(200) NOT NULL default '',
  `created` TIMESTAMP NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY (`iduser`) )
ENGINE = InnoDB;

INSERT INTO `phalcon-tests`.`user` SET name='Atanas Beloborodov', username='n@sk0', email='nask0@cod3r.net', password='1234';
INSERT INTO `phalcon-tests`.`user` SET name='John Doe', username='jdoe', email='john.doe@cod3r.net', password='1234';

USE `phalcon-tests`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;